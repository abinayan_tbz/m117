<h1>M117 Portfolio</h1>

## Inhaltsverzeichnis
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Kabel 🌐💻](#kabel-)
  - [Funktionsweise des WLANs 📡](#funktionsweise-des-wlans-)
    - [Unterschiede zwischen den Topologien ✅❌](#unterschiede-zwischen-den-topologien-)
    - [Spezialfall: Meshtopologie 🔄⭐](#spezialfall-meshtopologie-)
  - [Draht, Litze und Glas 📶💡](#draht-litze-und-glas-)
- [ISP 🌐](#isp-)
  - [Direktanschluss ans Core-Network 🌐💡](#direktanschluss-ans-core-network-)
  - [Anschlussgerät ans Access-Network des Internet-Service-Providers (ISP) 📶🔌](#anschlussgerät-ans-access-network-des-internet-service-providers-isp-)
  - [Mein Internet Service Provider 🏠💻](#mein-internet-service-provider-)
      - [Technologie (ADSL, SDSL, VDSL, Glasfaser, Koaxialkabel, Drahtlos?)](#technologie-adsl-sdsl-vdsl-glasfaser-koaxialkabel-drahtlos)
    - [Welche Leistung (Datendurchsatzrate Up- und Download) wird zu welchem Preis angeboten? (Best Effort?)](#welche-leistung-datendurchsatzrate-up--und-download-wird-zu-welchem-preis-angeboten-best-effort)
    - [Wird der Router kostenlos mitgeliefert?](#wird-der-router-kostenlos-mitgeliefert)
    - [Einmalige Gebühren wie z.B. Aufschaltgebühren, Installationskosten etc.](#einmalige-gebühren-wie-zb-aufschaltgebühren-installationskosten-etc)
    - [Support, Hotline-Verfügbarkeit, kostenpflichtige (?) Vor-Ort-Servicedienstleistungen](#support-hotline-verfügbarkeit-kostenpflichtige--vor-ort-servicedienstleistungen)
    - [Vertragsdauer, Kündigungsfristen, Ansprechpartner](#vertragsdauer-kündigungsfristen-ansprechpartner)
    - [Netz-Verfügbarkeit (7x24h \* 365 Tage?) Wartungsintervalle etc.](#netz-verfügbarkeit-7x24h--365-tage-wartungsintervalle-etc)
    - [Skalierbarkeit (Wenn die Leistung nicht mehr reicht und der „Hahn aufgedreht“ werden muss.)](#skalierbarkeit-wenn-die-leistung-nicht-mehr-reicht-und-der-hahn-aufgedreht-werden-muss)
- [IP 🔢](#ip-)
  - [Bestimmen sie von der folgenden IP-Adresse die Netz-ID und Host-ID: 192.168.3.37/24 🖥️](#bestimmen-sie-von-der-folgenden-ip-adresse-die-netz-id-und-host-id-19216833724-️)
  - [Geben Sie für die folgende IP-Adresse die Netzwerkadresse und Broadcastadresse an: 78.23.49.123/255.255.255.0 🖥️](#geben-sie-für-die-folgende-ip-adresse-die-netzwerkadresse-und-broadcastadresse-an-7823491232552552550-️)
  - [Wie beurteilen Sie diese Adresse: 78.256.125.12/255.255.248.0 ❌](#wie-beurteilen-sie-diese-adresse-78256125122552552480-)
- [Reflexion](#reflexion)
  - [26.05.2023 - Einführung \& Kabeln](#26052023---einführung--kabeln)

## Kabel 🌐💻

### Funktionsweise des WLANs 📡

Das heutige WLAN ermöglicht die parallele Übertragung mehrerer Datenströme über verschiedene Kabel. Dabei können nur der Absender und der Empfänger die Übertragung sehen, im Gegensatz zu älteren WLAN-Technologien, bei denen alle Teilnehmer im Netzwerk mithören konnten.

#### Unterschiede zwischen den Topologien ✅❌

**Vorteile:**

- Bei der Bustopologie haben andere Clients keine Probleme, wenn einer ausfällt.
- Bei der Meshtopologie haben die Clients immer noch Verbindung zueinander, selbst wenn ein Switch ausfällt.
- Bei der Sterntopologie treten keine Probleme auf, wenn ein Client ausfällt.

**Nachteile:**

- Bei der Ringtopologie bricht die Verbindung zusammen, wenn ein Client ausfällt.
- Bei der Baumtopologie haben die anderen Switches keine Verbindung zueinander, wenn ein Switch ausfällt.
- Bei der Sterntopologie gibt es keine Verbindung zwischen den Clients, wenn der Switch ausfällt.

#### Spezialfall: Meshtopologie 🔄⭐

Die Meshtopologie ist eine Mischung aus der Sterntopologie und der Ringtopologie. Dabei sind alle Switches miteinander verbunden, und von jedem Switch aus sind mehrere Clients angebunden.

### Draht, Litze und Glas 📶💡

Was sind die Vor- und Nachteile der drei unterschiedlichen Fabrikate?

## ISP 🌐

Um einen Zugang zum Internet herzustellen, benötigt man eine Verbindung zum World-Wide-Web (WWW). Dies erfolgt normalerweise über das Local Area Network (LAN) und den Übergang ins Wide Area Network (WAN) oder Core-Network.

Es gibt verschiedene Möglichkeiten, diese Verbindung herzustellen:

### Direktanschluss ans Core-Network 🌐💡

Ein direkter Glasfaseranschluss ermöglicht den Zugang zum Core-Network (Provider-Backbone). Das Core-Network ist das Hauptnetzwerk, das von den Betreibern der Verbindungsnetzwerke unterhalten wird.

### Anschlussgerät ans Access-Network des Internet-Service-Providers (ISP) 📶🔌

Eine häufige Methode, ins Internet zu gelangen, ist der Anschluss an das Access-Network eines Internet-Service-Providers wie Swisscom oder Sunrise/UPC. Dafür benötigt man entsprechendes Anschlussgerät in Ihrem LAN, das Sie mit dem ISP verbindet.

Dieses Anschlussgerät werden grundsätzlich von der ISP geliefert.

### Mein Internet Service Provider 🏠💻

##### Technologie (ADSL, SDSL, VDSL, Glasfaser, Koaxialkabel, Drahtlos?)
- Glasfaser

#### Welche Leistung (Datendurchsatzrate Up- und Download) wird zu welchem Preis angeboten? (Best Effort?)
- 

#### Wird der Router kostenlos mitgeliefert?
- Ja, der Router wird mitgeliefert

#### Einmalige Gebühren wie z.B. Aufschaltgebühren, Installationskosten etc.
- Ja, es gibt einen gewissen Betrag den man für das Aufschalten bezahlen muss.

#### Support, Hotline-Verfügbarkeit, kostenpflichtige (?) Vor-Ort-Servicedienstleistungen
- Es hat Hotline, welches kostenlos ist

#### Vertragsdauer, Kündigungsfristen, Ansprechpartner
- 

#### Netz-Verfügbarkeit (7x24h * 365 Tage?) Wartungsintervalle etc.
- Das Netz ist sehr zuverlässig, es ist immer verfügbar. Jedoch gibt es paar Ausnahmen.

#### Skalierbarkeit (Wenn die Leistung nicht mehr reicht und der „Hahn aufgedreht“ werden muss.)
- 

## IP 🔢

### Bestimmen sie von der folgenden IP-Adresse die Netz-ID und Host-ID: 192.168.3.37/24 🖥️

Bei der IP-Adresse 192.168.3.37/24 handelt es sich um eine IP-Adresse der Klasse C mit einer Subnetzmaske von 24 Bits, was einer Subnetzmaske von 255.255.255.0 entspricht. Um die Netz-ID und die Host-ID zu bestimmen, müssen wir die IP-Adresse und die Subnetzmaske bitweise verknüpfen.

Die Netz-ID ergibt sich durch die bitweise UND-Verknüpfung der IP-Adresse und der Subnetzmaske:

192.168.3.37 (11000000.10101000.00000011.00100101) 🌐

Subnetzmaske 255.255.255.0 (11111111.11111111.11111111.00000000) 🎭

Netz-ID: 192.168.3.0

Die Host-ID ergibt sich aus dem Rest der IP-Adresse:

Host-ID: 0.0.0.37

Die Netz-ID ist 192

.168.3.0 und die Host-ID ist 0.0.0.37.

### Geben Sie für die folgende IP-Adresse die Netzwerkadresse und Broadcastadresse an: 78.23.49.123/255.255.255.0 🖥️

Bei der IP-Adresse 78.23.49.123/255.255.255.0 handelt es sich erneut um eine IP-Adresse der Klasse C mit einer Subnetzmaske von 24 Bits.

Die Netzwerkadresse erhält man, indem man die bitweise UND-Verknüpfung der IP-Adresse und der Subnetzmaske durchführt:

78.23.49.123 (01001110.00010111.00110001.01111011) 🌐

Subnetzmaske 255.255.255.0 (11111111.11111111.11111111.00000000) 🎭

Netzwerkadresse: 78.23.49.0

Die Broadcastadresse kann man bestimmen, indem man alle Bits in der Host-ID auf 1 setzt:

Host-ID: 0.0.0.123

Broadcastadresse: 78.23.49.255

Die Netzwerkadresse ist 78.23.49.0 und die Broadcastadresse ist 78.23.49.255.

### Wie beurteilen Sie diese Adresse: 78.256.125.12/255.255.248.0 ❌

Die angegebene IP-Adresse 78.256.125.12/255.255.248.0 ist ungültig. Die Oktette einer IP-Adresse können Werte von 0 bis 255 haben. In diesem Fall ist das zweite Oktett "256", was über dem gültigen Bereich liegt. Daher handelt es sich um eine ungültige IP-Adresse.

## Reflexion

### 26.05.2023 - Einführung & Kabeln

Heute habe ich die Grundlagen der Netzwerktechnologie erforscht, einschließlich Übertragungsmodi, Topologien und Übertragungsmedien wie Draht, Litze und Glas. Ich habe gelernt, wie Störeinflüsse abgewehrt werden können und welche Bedeutung Abschirmungen und Twisted-Pair-Kabel haben. Zudem wurde mir die Bedeutung von Kabelkategorien wie CAT7 und Ethernet-Medientypen wie 1000Base-T vermittelt. Abschließend habe ich die universelle Gebäudeverkabelung (UGV) kennengelernt, bei der Draht für stabile Verbindungen eingesetzt wird.